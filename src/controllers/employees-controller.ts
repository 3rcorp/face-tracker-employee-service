import * as express from 'express';
import Controller from '../interfaces/controller-interface';
import Employee from '../interfaces/employee-interface';
import * as DatabaseService from '../services/database-service';
import * as FaceService from '../services/face-service';
import validator from 'validator';
import { cpf as CpfValidator } from 'cpf-cnpj-validator';

export default class EmployeesController implements Controller {
  public path = '/employees';
  public router = express.Router();

  constructor() {
    this.router.get(this.path, this.getEmployees.bind(this));
    this.router.post(this.path, this.createEmployee.bind(this));
    this.router.put(`${this.path}/:employeeId`, this.updateEmployee.bind(this));
    this.router.delete(
      `${this.path}/:employeeId`,
      this.deleteEmployee.bind(this)
    );
    this.router.put(
      `${this.path}/:employeeId/face`,
      this.updateFace.bind(this)
    );
  }

  private async getEmployees(req: express.Request, res: express.Response) {
    const employees = await DatabaseService.getEmployeesByClient(req.client);
    res.json(employees);
  }

  private async createEmployee(req: express.Request, res: express.Response) {
    let employee = this.getEmployeeFromRequest(req);
    try {
      if (!(await this.validateEmployeeFields(req.client, employee, res))) {
        return;
      }

      employee = await DatabaseService.createEmployee(req.client, employee);
      res.status(200).json(employee);
    } catch (error) {
      console.error(error);
      res.sendStatus(500);
    }
  }

  private async updateEmployee(req: express.Request, res: express.Response) {
    const employee = this.getEmployeeFromRequest(req);
    try {
      if (!(await this.validateEmployeeFields(req.client, employee, res))) {
        return;
      }

      await DatabaseService.updateEmployee(req.client, employee);
      res.status(200).json(employee);
    } catch (error) {
      console.error(error);
      res.sendStatus(500);
    }
  }

  private async deleteEmployee(req: express.Request, res: express.Response) {
    const employee = this.getEmployeeFromRequest(req);
    try {
      await FaceService.deletePeopleByClientAndEmployee(req.client, employee);
      await DatabaseService.deleteEmployee(req.client, employee);
      res.sendStatus(200);
    } catch (error) {
      console.error(error);
      res.sendStatus(500);
    }
  }

  private updateFace(req: express.Request, res: express.Response) {
    const employee = this.getEmployeeFromRequest(req);
    let data = Buffer.from('');
    req.on('data', (chunk) => {
      data = Buffer.concat([data, chunk]);
    });
    req.on('end', async () => {
      try {
        employee.personId = await FaceService.detectAndRecognizeFace(
          req.client,
          employee,
          data
        );

        if (employee.personId) {
          await DatabaseService.updateEmployeePersonId(req.client, employee);
          res.sendStatus(200);
        } else {
          res.status(418).send('Face not detected');
        }
      } catch (error) {
        console.error(error);
        res.sendStatus(500);
      }
    });
  }

  private getEmployeeFromRequest(req: express.Request): Employee {
    return {
      Id: req.params.employeeId,
      firstName: req.body.firstName || '',
      lastName: req.body.lastName || '',
      cpf: req.body.cpf || '',
      rg: req.body.rg || '',
      email: req.body.email || undefined,
    };
  }

  private async validateEmployeeFields(
    client: string,
    employee: Employee,
    res: express.Response
  ): Promise<boolean> {
    let problems = {};
    if (validator.isEmpty(employee.firstName)) {
      problems = { ...problems, firstName: 'Obrigatório' };
    } else if (!validator.isLength(employee.firstName, { max: 50 })) {
      problems = { ...problems, firstName: 'Muito grande' };
    }

    if (validator.isEmpty(employee.lastName)) {
      problems = { ...problems, lastName: 'Obrigatório' };
    } else if (!validator.isLength(employee.lastName, { max: 50 })) {
      problems = { ...problems, lastName: 'Muito grande' };
    }

    if (validator.isEmpty(employee.cpf)) {
      problems = { ...problems, cpf: 'Obrigatório' };
    } else if (!CpfValidator.isValid(employee.cpf)) {
      problems = { ...problems, cpf: 'Inválido' };
    } else if (
      await DatabaseService.checkEmployeeWithSameCPF(
        client,
        employee.cpf,
        employee.Id
      )
    ) {
      problems = { ...problems, cpf: 'CPF já foi usado' };
    }

    if (validator.isEmpty(employee.rg)) {
      problems = { ...problems, rg: 'Obrigatório' };
    } else if (!validator.isLength(employee.rg, { max: 50 })) {
      problems = { ...problems, rg: 'Muito grande' };
    }

    if (employee.email && !validator.isEmail(employee.email)) {
      problems = { ...problems, email: 'Email inválido' };
    }

    if (Object.keys(problems).length > 0) {
      res.status(400).json(problems);
      return false;
    }

    return true;
  }
}
