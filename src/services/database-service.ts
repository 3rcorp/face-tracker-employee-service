import * as MSSQL from 'mssql';
import Employee from '../interfaces/employee-interface';

let connectionPool: MSSQL.ConnectionPool;

export async function initialize(): Promise<void> {
  const { SQLSERVER_CONNECTION_STRING } = process.env;
  if (!SQLSERVER_CONNECTION_STRING) {
    console.error('SQLSERVER_CONNECTION_STRING not defined.');
    process.exit(1);
  }

  try {
    connectionPool = await MSSQL.connect(
      JSON.parse(SQLSERVER_CONNECTION_STRING)
    );
  } catch (error) {
    console.error('Could not connect into SQL database:', error.message);
    process.exit(1);
  }
}

export async function getClientByEmailAndPassword(
  email: string,
  password: string
): Promise<string | void> {
  const result = await connectionPool
    .request()
    .input('email', email)
    .input('password', password)
    .query(
      'SELECT client FROM all_users WHERE email = @email AND user_pw = @password'
    );
  return result?.recordset[0]?.client;
}

export async function getClientIdByName(name: string): Promise<string | void> {
  const result = await connectionPool
    .request()
    .input('client', name)
    .query(
      'SELECT TOP 1 Id FROM clients WHERE client = @client ORDER BY Id ASC'
    );

  return result?.recordset[0]?.Id;
}

export async function getEmployeesByClient(
  client: string
): Promise<Employee[] | void> {
  const result = await connectionPool
    .request()
    .input('client', client)
    .query(
      `SELECT Id, cpf, rg, first_name, last_name, email
        FROM Faces_client WHERE client = @client
        ORDER BY first_name, last_name`
    );
  return result.recordset.map((recordset) => ({
    Id: recordset.Id,
    firstName: recordset.first_name,
    lastName: recordset.last_name,
    cpf: recordset.cpf,
    rg: recordset.rg,
    email: recordset.email,
  }));
}

export async function checkEmployeeWithSameCPF(
  client: string,
  cpf: string,
  ignoreId?: string
): Promise<boolean> {
  const query = ignoreId
    ? `SELECT 1 AS exist FROM Faces_client 
        WHERE client = @client and cpf = @cpf AND Id <> @id`
    : `SELECT 1 AS exist FROM Faces_client
        WHERE client = @client and cpf = @cpf`;

  let request = connectionPool
    .request()
    .input('client', client)
    .input('cpf', cpf);

  if (ignoreId) {
    request = request.input('id', ignoreId);
  }

  const result = await request.query(query);
  return !!result.recordset[0]?.exist;
}

export async function createEmployee(
  client: string,
  employee: Employee
): Promise<Employee> {
  const query = `
    INSERT INTO Faces_client (first_name, last_name, cpf, rg, email, client)
    OUTPUT inserted.Id
    VALUES (@firstName, @lastName, @cpf, @rg, @email, @client)
  `;

  const result = await connectionPool
    .request()
    .input('firstName', employee.firstName)
    .input('lastName', employee.lastName)
    .input('cpf', employee.cpf)
    .input('rg', employee.rg)
    .input('email', employee.email)
    .input('client', client)
    .query(query);

  employee.Id = result.recordset[0]?.Id;

  return employee;
}

export async function updateEmployee(
  client: string,
  employee: Employee
): Promise<unknown> {
  const query = `
    UPDATE Faces_client SET
      first_name = @firstName,
      last_name = @lastName,
      cpf = @cpf,
      rg = @rg,
      email = @email
    WHERE Id = @id AND client = @client
  `;

  if (!employee.Id) {
    throw new Error('ID not defined');
  }

  return connectionPool
    .request()
    .input('id', employee.Id)
    .input('firstName', employee.firstName)
    .input('lastName', employee.lastName)
    .input('cpf', employee.cpf)
    .input('rg', employee.rg)
    .input('email', employee.email)
    .input('client', client)
    .query(query);
}

export async function deleteEmployee(
  client: string,
  employee: Employee
): Promise<unknown> {
  if (!employee.Id) {
    throw new Error('ID not defined');
  }

  const query = `
    DELETE FROM Faces_client WHERE Id = @id AND client = @client
  `;

  return connectionPool
    .request()
    .input('id', employee.Id)
    .input('client', client)
    .query(query);
}

export async function updateEmployeePersonId(
  client: string,
  employee: Employee
): Promise<unknown> {
  const query = `
    UPDATE Faces_client SET
      person_id = @personId
    WHERE Id = @id AND client = @client
  `;

  if (!employee.Id) {
    throw new Error('ID not defined');
  }

  if (!employee.personId) {
    throw new Error('Person ID not defined');
  }

  return connectionPool
    .request()
    .input('id', employee.Id)
    .input('personId', employee.personId)
    .input('client', client)
    .query(query);
}
