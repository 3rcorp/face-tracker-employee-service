import {
  FaceClient,
  PersonGroupOperations,
  PersonGroupPerson,
} from '@azure/cognitiveservices-face';
import { CognitiveServicesCredentials } from '@azure/ms-rest-azure-js';
import Employee from '../interfaces/employee-interface';
import * as DatabaseService from './database-service';

let faceClient: FaceClient;
let personGroupClient: PersonGroupOperations;
let personGroupPersonClient: PersonGroupPerson;

export async function initialize(): Promise<void> {
  const { COGNITIVE_API_KEY, COGNITIVE_API_ENDPOINT } = process.env;

  if (!COGNITIVE_API_KEY) {
    console.error('COGNITIVE_API_KEY not defined.');
    process.exit(1);
  }

  if (!COGNITIVE_API_ENDPOINT) {
    console.error('COGNITIVE_API_ENDPOINT not defined.');
    process.exit(1);
  }

  const credentials = new CognitiveServicesCredentials(COGNITIVE_API_KEY);
  faceClient = new FaceClient(credentials, COGNITIVE_API_ENDPOINT);
  personGroupClient = new PersonGroupOperations(faceClient);
  personGroupPersonClient = new PersonGroupPerson(faceClient);
}

export async function detectAndRecognizeFace(
  client: string,
  employee: Employee,
  imageContent: Buffer
): Promise<string | undefined> {
  const faceId = await detectFaceId(imageContent);
  if (!faceId) {
    return;
  }
  const personGroupId = await createPersonGroup(client);
  await deletePeopleByEmployee(personGroupId, employee);
  return createPersonAndTrainGroup(personGroupId, imageContent, employee);
}

async function detectFaceId(imageContent: Buffer): Promise<string | void> {
  const options = {
    returnFaceId: true,
    returnFaceLandmarks: false,
  };

  const response = await faceClient.face.detectWithStream(
    imageContent,
    options
  );

  return response[0]?.faceId;
}

async function createPersonGroup(client: string): Promise<string> {
  const personGroupId = await getPersonGroupId(client);
  try {
    await personGroupClient.create(personGroupId, { name: client });
    console.log(`Person group ${personGroupId} created`);
  } catch (error) {
    if (error.code !== 'PersonGroupExists') {
      throw error;
    }
  }
  return personGroupId;
}

async function createPersonAndTrainGroup(
  personGroupId: string,
  imageContent: Buffer,
  employee: Employee
) {
  const personId = await createPerson(personGroupId, employee);
  await addFaceToPerson(personGroupId, personId, imageContent);
  await trainPersonGroup(personGroupId);
  return personId;
}

export async function deletePeopleByClientAndEmployee(
  client: string,
  employee: Employee
) {
  const personGroupId = await getPersonGroupId(client);
  return deletePeopleByEmployee(personGroupId, employee);
}

async function deletePeopleByEmployee(
  personGroupId: string,
  employee: Employee
) {
  const response = await personGroupPersonClient.list(personGroupId);
  for (const entry of response) {
    if (entry.name === employee.Id) {
      await deletePerson(personGroupId, entry.personId);
    }
  }
}

async function deletePerson(personGroupId: string, personId: string) {
  await personGroupPersonClient.deleteMethod(personGroupId, personId);
  console.log(`Removed person ${personId} from ${personGroupId}`);
}

async function createPerson(
  personGroupId: string,
  employee: Employee
): Promise<string> {
  const response = await personGroupPersonClient.create(personGroupId, {
    name: employee.Id,
  });
  const { personId } = response;
  console.log(`Person created ${personId} in group ${personGroupId}`);
  return personId;
}

async function addFaceToPerson(
  personGroupId: string,
  personId: string,
  imageContent: Buffer
): Promise<string> {
  const response = await personGroupPersonClient.addFaceFromStream(
    personGroupId,
    personId,
    imageContent
  );
  const { persistedFaceId } = response;
  console.log(
    `Face image added ${persistedFaceId} into person ${personId} of group ${personGroupId}`
  );
  return persistedFaceId;
}

async function trainPersonGroup(personGroupId: string) {
  await personGroupClient.train(personGroupId);
  console.log(`Person group ${personGroupId} asked to train`);
}

async function getPersonGroupId(client: string): Promise<string> {
  const clientId = await DatabaseService.getClientIdByName(client);

  if (!clientId) {
    throw new Error(`No client ${client} found.`);
  }

  return `client-${clientId}`;
}
