export default interface Employee {
  Id?: string;
  firstName: string;
  lastName: string;
  cpf: string;
  rg: string;
  email?: string;
  personId?: string;
}
