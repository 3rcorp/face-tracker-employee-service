import { Request, Response, NextFunction } from 'express';
import * as DatabaseService from '../services/database-service';

export default async function authenticationHandler(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const email = req.query.email as string;
  const pw = req.query.pw as string;

  if (!email || !pw) {
    res.sendStatus(401);
    return;
  }

  try {
    const client = await DatabaseService.getClientByEmailAndPassword(email, pw);
    if (!client) {
      res.sendStatus(401);
      return;
    } else {
      req.client = client;
    }
  } catch (error) {
    console.error('Could not authenticate:', error.message);
    res.sendStatus(500);
    return;
  }

  next();
}
