import express from 'express';
import cors from 'cors';
import EmployeesController from './controllers/employees-controller';
import authenticationHandler from './middleware/authentication-handler';
import * as DatabaseService from './services/database-service';
import * as FaceService from './services/face-service';
require('dotenv-defaults').config();

let app: express.Express | null = null;

main().catch((error) => console.error(error));

async function main() {
  await DatabaseService.initialize();
  await FaceService.initialize();
  await startWebserver();
}

async function startWebserver() {
  app = express();
  app.get('/', (req, res) => res.end('PONG!'));

  app.use(cors());
  app.use(express.json());
  app.use(authenticationHandler);

  app.use('/', new EmployeesController().router);

  app.listen(process.env.HTTP_PORT, () => {
    console.log(`Server is running at ${process.env.HTTP_PORT} port`);
  });
}
